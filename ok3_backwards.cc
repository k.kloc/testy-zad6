#include <cassert>
#include <iostream>
#include <memory>
#include <sstream>
#include "../rover.h"

struct FalseSensor : public Sensor {
    bool is_safe([[maybe_unused]] coordinate_t x,
                 [[maybe_unused]] coordinate_t y) override {
        return false;
    }
};

std::string get_string_in_ostream(const auto &rover) {
	std::stringstream s;
	s << rover;
	return s.str();
}

int main() {
	auto rover = RoverBuilder()
            .program_command('B', move_backward())
			.add_sensor(std::make_unique<FalseSensor>())
			.build();
	rover.land({0, 0}, Direction::NORTH);
	rover.execute("B");
	assert(get_string_in_ostream(rover) == "(0, 0) NORTH stopped");
}
